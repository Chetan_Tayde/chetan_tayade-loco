const express = require('express');

const app = express();
const bodyParser = require('body-parser');
const transactionRoutes = require('./routes/transactions');


app.use(bodyParser.json({ extended: true }));
app.use(bodyParser.urlencoded({ extended: true }));


app.get('/', (req, res) => {
  res.send('Hello World');
});


// Routess/
app.all('*', transactionRoutes);


// server listening on
app.listen(3000, () => {
  console.log('server successfully started on 3000');
});

module.exports = app;
