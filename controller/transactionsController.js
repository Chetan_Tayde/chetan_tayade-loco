

const transactionsData = [];


function createTransactions(req, res) {
  const  {transactionId}  = req.params;
  const reqBody = req.body;
  reqBody.transactionId = transactionId;
  const picked = transactionsData.find(o => o.transactionId === transactionId);
  if (picked == null) {
    transactionsData.push(reqBody);
    res.json({ status: 'ok' });
  } else {
    res.json({ success: true, status: 200, message: 'transaction id is already present' });
  }
}


function getTransactionsbyid(req, res) {
  const { transactionId } = req.params;
  const picked = transactionsData.find(o => o.transactionId === transactionId);
  if (picked == null) {
    res.send({ success: true, status: 200, message: 'transaction details not found' });
  }
  res.status(200).send(picked);
}


function getTransactionsbyTypes(req, res) {
  const { type } = req.params;
  const newVal = transactionsData.find(obj => obj.type === type);
  if (newVal == null) {
    res.send({ success: true, status: 200, message: 'transaction types not found' });
  }
  res.status(200).send(newVal);
}


function getSumByid(req, res) {
  const { transactionId } = req.params;
  let sum;
  const finalData = transactionsData.filter(obj => obj.transactionId === transactionId);
  const amountList = finalData.map(transaction => transaction.amount);
  if (amountList.length) {
    sum = amountList.reduce((a, b) => a + b, 0);
  }
  res.json({ sum });
}

module.exports.getTransactionsbyid = getTransactionsbyid;
module.exports.createTransactions = createTransactions;
module.exports.getTransactionsbyTypes = getTransactionsbyTypes;
module.exports.getSumByid = getSumByid;
