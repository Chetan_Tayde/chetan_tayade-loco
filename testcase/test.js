const expect = require('chai').expect;
const request = require('supertest');
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');

const should = chai.should();
chai.use(chaiHttp);


it('Main page content', (done) => {
  request('/', (error, response, body) => {
    expect(body).to.equal('Hello World');
  });
  done();
});


describe.only('create  transacation', () => {
  it.only('it should create a transacation by given transacation_id', (done) => {
    chai.request(server)
      .put('/api/transactionservice/transaction/21')
      .send({ amount: 50, type: 'taxi' })
      .end((err, res) => {
        res.should.have.status(200);
        console.log('========>>>', res.body);
        done();
      });
  });
});


describe.only('transacation_id already Exist', () => {
  it.only('it should not create a transacation by given transacation_id', (done) => {
    chai.request(server)
      .put('/api/transactionservice/transaction/21')
      .send({ amount: 50, type: 'taxi' })
      .end((err, res) => {
        res.should.have.status(200);
        console.log('========>>>', res.body);
        done();
      });
  });
});


describe.only('Get Transaction by types', () => {
  it.only('it should return transaction by types', (done) => {
    chai.request(server)
      .get('/api/transactionservice/types/taxi')
      .end((err, res) => {
        res.should.have.status(200);
        console.log('========>>>', res.body);
        done();
      });
  });
});


describe.only('Get Transaction by types', () => {
  it.only('it should return transaction by types', (done) => {
    chai.request(server)
      .get('/api/transactionservice/types/taxia')
      .end((err, res) => {
        res.should.have.status(200);
        console.log('========>>>', res.body);
        done();
      });
  });
});


describe.only('Get Transaction by transaction_id', () => {
  it.only('it should return transaction by transaction_id', (done) => {
    chai.request(server)
      .get('/api/transactionservice/gettransactionbyid/21')
      .end((err, res) => {
        res.should.have.status(200);
        console.log('===', res.body);
        done();
      });
  });
});


describe.only('transaction_id is not present', () => {
  it.only('it should not return transaction', (done) => {
    chai.request(server)
      .get('/api/transactionservice/gettransactionbyid/200')
      .end((err, res) => {
        res.should.have.status(200);
        console.log('===', res.body);
        done();
      });
  });
});


describe.only('transaction_id is not present', () => {
  it.only('it should not return sum of account', (done) => {
    chai.request(server)
      .get('/api/transactionservice/sum/220')
      .end((err, res) => {
        res.should.have.status(200);
        console.log('===', res.body);
        done();
      });
  });
});


describe.only('Get sum of transaction by transaction_id ', () => {
  it.only('it should  return sum ', (done) => {
    chai.request(server)
      .get('/api/transactionservice/sum/21')
      .end((err, res) => {
        res.should.have.status(200);
        console.log('===', res.body);
        done();
      });
  });
});
