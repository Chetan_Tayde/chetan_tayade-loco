const express = require('express');

const router = express.Router();

const transactionsController = require('../controller/transactionsController');


// route

router.put('/api/transactionservice/transaction/:transactionId', transactionsController.createTransactions);
router.get('/api/transactionservice/gettransactionbyid/:transactionId', transactionsController.getTransactionsbyid);
router.get('/api/transactionservice/types/:type', transactionsController.getTransactionsbyTypes);
router.get('/api/transactionservice/sum/:transactionId', transactionsController.getSumByid);


// exports route
module.exports = router;
